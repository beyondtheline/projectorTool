# ProjectorTool

Rhino and Grasshopper definition for projector simulation
(Rhino, Grasshopper, Projection)

## Features

- Define projector beam with 
    - Projection ratio (min. / max.)
    - Projection distance
    - Zoom between ratios
    - Aspect Ratio
    - Projection distance
- Calculation of projection width and height
- Intersection with defined projection surface
- Mirror the projection beam

## Usage

1. Draw a circle in the size of the lens and set it as origin in grasshopper 
2. Set the specs of your projector
    - Projection distance
    - Projection Ratio min.
    - Projection Ratio max.
    - Zoom between ratios 
    - Aspect Ratio
3. Set a Brep as projection surface  
4. (optional) define a mirror to refract the beam